<?php

namespace Drupal\mastodon\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Mastodon share button' block.
 *
 * @Block(
 *  id = "mastodon_share_button",
 *  admin_label = @Translation("Mastodon share button"),
 * )
 */
class ShareButton extends BlockBase {

  // @todo make this block configurable.

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#type' => 'inline_template',
      '#template' => '<share-on-mastodon></share-on-mastodon>',
      '#attached' => [
        'library' => [
          'mastodon/share_button',
        ],
      ],
    ];
  }

}
