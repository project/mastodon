## Lightweight share and embed

No authentication or API configuration needed.

- Share button
- Embed: list of posts or a specific one

Relies on these projects
- [share-on-mastodon](https://github.com/micahilbery/share-on-mastodon)
- [fed-embed](https://github.com/andy-blum/fed-embed)

Provided as configurable Drupal Blocks for the first implementation.
WYSIWYG / Gutenberg / oEmbed / ... integration to be provided in the next implementations.

## API support

Based on this [API wrapper](https://packagist.org/packages/colorfield/mastodon-api).

It can be used without authentication to get [publicly available data](https://docs.joinmastodon.org/client/public/).

For other requests, configure OAuth.

### Using the API

The Mastodon class is available as a Drupal service that uses OAuth
credentials defined from the configuration.
This is still a WIP so the Drupal interface is subject to change during the alpha versions.

Examples

```php
$mastodon = \Drupal::service('mastodon.api');
$user = $mastodon->authenticateUser($email, $password);
// ...
$followers = $mastodon->getApi()->get('/accounts/'.$user_id.'/followers');
```

It is also possible to use the API as is, without the Drupal integration.

[API Documentation](https://github.com/colorfield/mastodon-api-php)

## Roadmap

- Syntactic sugar over the API wrapper for common endpoints
- Make it possible to not use OAuth for public data with the Drupal service (already possible via the API package)
- Document optional OAuth configuration via environment variables
- Alternate embed options for editors (WYSIWYG, ...)
